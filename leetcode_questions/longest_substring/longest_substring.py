from collections import defaultdict


def lengthOfLongestSubstring(s: str) -> int:
    len_max = 0
    all_sub_strings = []
    for i in range(len(s) + 1):
        for j in range(i + 1, len(s) + 1):
            if len(set(s[i:j])) == len(s[i:j]) and len(s[i:j]) > len_max:
                len_max = len(s[i:j])
    return len_max


if __name__ == '__main__':
    s = "pwwkew"
    print(lengthOfLongestSubstring(s))
