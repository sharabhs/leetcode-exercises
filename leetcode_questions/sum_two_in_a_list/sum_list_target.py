from collections import Counter
from itertools import combinations
from typing import List


def sum_target(a: List, target: int):
    tuples_lst = list(combinations(a, 2))
    sum_tuples_lst = list(sum(x) for x in tuples_lst)
    idx = sum_tuples_lst.index(target)
    return [a.index(x) for x in tuples_lst[idx]]


if __name__ == '__main__':
    nums = [3,3]
    target = 6
    print(sum_target(nums, target))
