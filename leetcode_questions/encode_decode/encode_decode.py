from typing import List


def encode_str(lst_str: List):
    encoded_str = ''
    for i, s in enumerate(lst_str):
        encoded_str += (str(len(s)) + '#' + s)
    return encoded_str


def decode_str(encoded_str: str):
    lst_str = []
    i = 0
    while i < len(encoded_str):
        if encoded_str[i].isnumeric() and encoded_str[i + 1] == '#':
            lst_str.append(encoded_str[i + 2: i + 2 + int(encoded_str[i])])
        i += int(encoded_str[i]) + 2
    return lst_str


if __name__ == '__main__':
    str_to_encode = ['bat', 'ball', '6#rty56', '2727383', 'wiw9wj9wj']
    print(encode_str(str_to_encode))
    encoded_str = encode_str(str_to_encode)
    decoded_str = decode_str(encoded_str)
    print(decoded_str)
