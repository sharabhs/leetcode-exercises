from collections import defaultdict
from pathlib import Path


def path_test():
    cwd = Path('.')
    cwd.touch('new_file.txt')
    print(cwd.absolute())


def group_analgrams(strs: list):
    res = defaultdict(list)

    for s in strs:
        count = [0] * 26

        for c in s:
            count[ord(c) - ord('a')] += 1
        res[tuple(count)].append(s)
    return list(res.values())


if __name__ == '__main__':
    strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
    print(group_analgrams(strs))
    #print(path_test())
