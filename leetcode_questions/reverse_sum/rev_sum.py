from typing import Optional, List


def addTwoNumbers(l1: Optional[List], l2: Optional[List]) -> Optional[List]:
    l1 = [str(x) for x in l1]
    l2 = [str(x) for x in l2]
    l1 = ''.join(l1)
    l2 = ''.join(l2)
    l3 = int(l1) + int(l2)
    return [int(x) for x in str(l3)[::-1]]


if __name__ == '__main__':
    l1 = [2, 4, 3]
    l2 = [5, 6, 4]
    print(addTwoNumbers(l1, l2))
