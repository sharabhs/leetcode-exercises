from itertools import count, islice, tee, repeat, chain

if __name__ == '__main__':
    v = count(0)
    print(list(islice(v, 5, 10)))

    v = repeat(10, 4)
    print(list(v))

    x = repeat([1,2,3,4,5], 10)
    y = chain(x)
    print(list(y))

